Bolwing Game Kata
=================

My first run through of Uncle Bob's Bowling Game Kata.

You can find his PowerPoint walkthrough
[here](http://butunclebob.com/files/downloads/Bowling%20Game%20Kata.ppt).
